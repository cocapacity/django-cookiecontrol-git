import os

try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

def read(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).read()

required = ['django>=1.2',]

setup(
	name='django-cookiecontrol',
	version='0.1.3',
	author='Maciek Szczesniak',
	author_email='maciek@id43.net',
	description=('Django integration for CookieControl script, forked from vvarp'),
	license="MIT",
	url='https://bitbucket.org/lhilarides/django-cookiecontrol',
	packages=['cookiecontrol'],
	install_requires=required,
	long_description=read('README.md'),
	include_package_data=True,
	classifiers=[
		'Development Status :: 3 - Alpha',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: MIT License',
		'Natural Language :: English',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 2.7'
	],
)
