# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CookieControlConfig'
        db.create_table('cookiecontrol_cookiecontrolconfig', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('intro_text', self.gf('django.db.models.fields.TextField')()),
            ('full_text', self.gf('django.db.models.fields.TextField')()),
            ('consent_model', self.gf('django.db.models.fields.CharField')(default='implicit', max_length=20)),
            ('position', self.gf('django.db.models.fields.CharField')(default='left', max_length=10)),
            ('shape', self.gf('django.db.models.fields.CharField')(default='diamond', max_length=10)),
            ('theme', self.gf('django.db.models.fields.CharField')(default='light', max_length=10)),
            ('start_open', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('auto_hide', self.gf('django.db.models.fields.IntegerField')(default=6)),
            ('subdomains', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('countries', self.gf('django.db.models.fields.TextField')(default='United Kingdom')),
            ('ga_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ga_id', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('addthis_allow_cookies', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('piwik_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('piwik_base_url', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('piwik_site_id', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
        ))
        db.send_create_signal('cookiecontrol', ['CookieControlConfig'])


    def backwards(self, orm):
        # Deleting model 'CookieControlConfig'
        db.delete_table('cookiecontrol_cookiecontrolconfig')


    models = {
        'cookiecontrol.cookiecontrolconfig': {
            'Meta': {'object_name': 'CookieControlConfig'},
            'addthis_allow_cookies': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'auto_hide': ('django.db.models.fields.IntegerField', [], {'default': '6'}),
            'consent_model': ('django.db.models.fields.CharField', [], {'default': "'implicit'", 'max_length': '20'}),
            'countries': ('django.db.models.fields.TextField', [], {'default': "'United Kingdom'"}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'ga_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ga_id': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro_text': ('django.db.models.fields.TextField', [], {}),
            'piwik_base_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'piwik_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'piwik_site_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'default': "'left'", 'max_length': '10'}),
            'shape': ('django.db.models.fields.CharField', [], {'default': "'diamond'", 'max_length': '10'}),
            'start_open': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'subdomains': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'theme': ('django.db.models.fields.CharField', [], {'default': "'light'", 'max_length': '10'})
        }
    }

    complete_apps = ['cookiecontrol']