# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        from django.core.management import call_command
        call_command("loaddata", "cookiecontrol_base_config.json")

    def backwards(self, orm):
        "Write your backwards methods here."
        pass

    models = {
        'cookiecontrol.cookiecontrolconfig': {
            'Meta': {'object_name': 'CookieControlConfig'},
            'addthis_allow_cookies': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'auto_hide': ('django.db.models.fields.IntegerField', [], {'default': '6'}),
            'consent_model': ('django.db.models.fields.CharField', [], {'default': "'implicit'", 'max_length': '20'}),
            'countries': ('django.db.models.fields.TextField', [], {'default': "'United Kingdom'"}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'ga_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ga_id': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro_text': ('django.db.models.fields.TextField', [], {}),
            'piwik_base_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'piwik_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'piwik_site_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'default': "'left'", 'max_length': '10'}),
            'shape': ('django.db.models.fields.CharField', [], {'default': "'diamond'", 'max_length': '10'}),
            'start_open': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'subdomains': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'theme': ('django.db.models.fields.CharField', [], {'default': "'light'", 'max_length': '10'})
        }
    }

    complete_apps = ['cookiecontrol']
    symmetrical = True
