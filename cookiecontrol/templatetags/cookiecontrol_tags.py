from cookiecontrol.models import CookieControlConfig
from django.template import Library
from django.template.context import Context

register = Library()

def cookiecontrol_js(context):
    return Context({'conf': CookieControlConfig.objects.all()[0]})

register.inclusion_tag("cookiecontrol/cookiecontrol.html", takes_context=True)(cookiecontrol_js)
