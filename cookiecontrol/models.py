from django.db import models

CCONTROL_CONSENT_MODEL = (
    ('implicit', 'implicit'),
    ('explicit', 'explicit'),
    ('information_only', 'information only')
)

CCONTROL_POSITION = (
    ('left', 'left'),
    ('right', 'right')
)

CCONTROL_SHAPE = (
    ('diamond', 'diamond'),
    ('triangle', 'triangle')
)

CCONTROL_THEME = (
    ('light', 'light'),
    ('dark', 'dark')
)

class CookieControlConfig(models.Model):

    intro_text = models.TextField(help_text="Some short, sharp copy to introduce the role of cookies on your site.")
    full_text = models.TextField(help_text="Describe in general terms what your cookies are used for.")

    consent_model = models.CharField(max_length=20, choices=CCONTROL_CONSENT_MODEL, default=CCONTROL_CONSENT_MODEL[0][0], help_text="Select the consent model you wish Cookie Control to use. In each consent model the Cookie Control panel appears to the user when they first access the site. <ul><li><strong>Information Only</strong>: Informs users the site is using cookies. They are given no option to opt out.</li><li><strong>Implicit</strong>: Informs users the site is using cookies and they are given the option to opt out.</li><li><strong>Explicit</strong>: Informs users the site would like to use cookies and they are given the option to opt in.</li></ul>")
    position = models.CharField(max_length=10, choices=CCONTROL_POSITION, default=CCONTROL_POSITION[0][0])
    shape = models.CharField(max_length=10, choices=CCONTROL_SHAPE, default=CCONTROL_SHAPE[0][0])
    theme = models.CharField(max_length=10, choices=CCONTROL_THEME, default=CCONTROL_THEME[0][0])

    start_open = models.BooleanField(default=True)
    auto_hide = models.IntegerField(default=6, help_text="In seconds.")
    subdomains = models.BooleanField(default=True, help_text="If you have multiple sub-domains, you can choose whether Cookie Control opt-ins apply across all sub-domains or just a single domain.")

    countries = models.TextField(default="United Kingdom, Netherlands", help_text="Please enter a COMMA SEPARATED list of countries for which you wish the plugin to appear. If left blank Cookie Control will appear for all users from all countries. The full list of countries can be found <a href='http://www.geoplugin.com/iso3166'>here</a>")

    ga_enabled = models.BooleanField(default=False)
    ga_id = models.CharField(max_length=15, blank=True)

    addthis_allow_cookies = models.BooleanField(default=False, help_text="Allow AddThis widget to use cookies.")

    piwik_enabled = models.BooleanField(default=False)
    piwik_base_url = models.CharField(max_length=200, blank=True, help_text="Piwik base URL without trailing slash, for example: http://stats.mydomain.com")
    piwik_site_id = models.CharField(max_length=10, blank=True, help_text="Piwik site ID, numeric value")

    def __unicode__(self):
        return u"Cookie Control Config"
