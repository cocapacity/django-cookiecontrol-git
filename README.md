Cookie Control widget integration for Django
============================================
>This django app can be used to comply (no guarantees, we're not lawyers) to the 2012 EU "cookie law". Basically what it does is two things:
>
> 1. Inform the user about cookies being set, or ask for permission to set these.
> 2. Create an admin plugin for configuration

>After installation the following steps are required:
>
>* Load initial data:

    :::bash
    python manage.py loaddata /PATH/TO/cookiecontrol/fixtures/cookiecontrol_base_config.json
>
>* Include the templatetag in your template, an empty template might look like this:

    :::html
    {% load cookiecontrol_tags %}
    <!doctype html>
    <html>
        <head>
        </head>
        <body>

        </body>
        {% cookiecontrol_js %}
    </html>
